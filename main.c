#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define H 50
#define W 190

/* 16 is an arbitrary number, it's just that
 * our system can handle up to 16 balls */

#define Max 16

//int Yoffsetpos = 0;
//int Xoffsetpos = H;
int frames = 0;
//float time = 0;
//float grav = -9.8;
//float vlctyX = 25;
//float vlctyY = 50;


/* properties of each ball (TODO: turn into structure) */
float vlctyX[Max];
float vlctyY[Max];
int Xoffsetpos[Max];
int Yoffsetpos[Max];
float time[Max];

/* current position of each ball */
int BallX[Max];
int BallY[Max];

/* Gravity is constant */
float grav = -9.8;

/* renderer current position */
int x, y;

/* # of balls */

int BallsInSystem = 5;

// TODO: The ultimate goal is to render multiple balls on screen at once that interact with each other and bounce off 
// of each other. I'd like to store force values relating to each ball, also current angles which the balls are at
// (tangent of parabola) so that when they bounce off each other, the direction in which the balls go can be calculated. 

// Turn all ball properties, grav,vlctyX,vlctyY into a structure for the property of a ball. 
// This will be done so that multiple balls can be rendered on screen each with there own corresponding
// values and such. Each property will have it's own malloc'd array so that it can grow and shrink with 
// the amount of balls there are. E.g if there are n # of balls, then the vlctyX malloc will be n big, as 
// each ball will need it's own vlctyX value. Each ball will also need to run XChkBounds() and YChkBounds() 
// although im not exactly sure how to implement that. In fact im not quite sure how to implement any of this.

int AccelerationEquX(float a, float t, float Vi){
  // d =  Vi * t + 1/2 * a * (t)^2
  return ((Vi * t) + ((0.5 * a) * pow(t, 2.0)));
}

int VelocityY(float v, float t){
  // d = Vf * t 
  return (v * t);
}

/* Checks if the ball hits the floor or not, and if it does it resets the time variable.
 * The time variable is only the time of one full parabola arc, not multiple; when the time is reset 
 * so is the full parabola. If the time were just reset over and over, what would show on screen is 
 * the repeated arc of a ball until infinity with no change in position what so ever. After it lands, it would
 * shoot back up from it's begining position and land at the same landing position. So to stop this 
 * Yoffsetpos is introduced to record the last landing spot, and then it's added to the arc equation so that
 * the ball starts from the landing position giving the illusion of it bouning and all being one equation, however 
 * each parabola is technically it's own equation. */
 

void XChkBounds(int ballnum)
{

  if( BallX[ballnum] < 0 ){      

    Yoffsetpos[ballnum] += BallY[ballnum];

    time[ballnum] = 0;                                    

  } 
  else if ( vlctyX <= 0 )
    exit(0);
}

void YChkBounds(int ballnum)
{
  int temp = Yoffsetpos[ballnum];

  if((BallY[ballnum] + Yoffsetpos[ballnum]) >= W)
  {
    // Flip velocity to negative if the right wall is hit so that the ball goes backwards
    
    // Temp is the number of last Yoffsetpos before ball hits the wall
    // Subtract temp from the width, then add that number back to width so that when
    // the ball hits the wall, it starts exactly from the wall.
    
   
    // This is done because when the velocity is flipped, it wants to push y (ignore Yoffsetpos)
    // right back to the last bounce, if y = 37 (VelocityY function, again ignore Yoffsetpos), when it hits the wall y 
    // will equal -37. If the wall is per se at 190, 190 - 37 = 153 which is the position where the ball 
    // last hit the floor (aka yoffsetpos). So in our case if 37 is added to the wall position at 190, it will 
    // negate the fact that the flipping of the velocity will make the ball jump in the other direction.
    
   // if( Yoffsetpos != 0 )
   // {
      Yoffsetpos[ballnum] = W + (W-temp); 
      vlctyY[ballnum] = (vlctyY[ballnum] * -1);
  //  }
  //   else
  //  {
  //    vlctyY = (vlctyY * -1) ;
  //  }

  } 

  // Re-flip velocity if the ball hits the right wall 

  else if (BallY[ballnum] + Yoffsetpos[ballnum] < 0 )
  {
    Yoffsetpos[ballnum] = 0 + (0-temp); 
    vlctyY[ballnum] = (vlctyY[ballnum] * -1);
  }

}

void initialize()
{

  /* Hard coded ball property values */

  // Ball 1
vlctyX[0] = 10;
vlctyY[0] = 10;
Xoffsetpos[0] = H;
Yoffsetpos[0] = 0;

  // Ball 2
vlctyX[1] = 20;
vlctyY[1] = 10;
Xoffsetpos[1] = H;
Yoffsetpos[1] = 0;

// Ball 3
vlctyX[2] = 30;
vlctyY[2] = 10;
Xoffsetpos[2] = H;
Yoffsetpos[2] = 0;

// Ball 4
vlctyX[3] = 20;
vlctyY[3] = 40;
Xoffsetpos[3] = H;
Yoffsetpos[3] = 0;

// Ball 5
vlctyX[4] = 15;
vlctyY[4] = 30;
Xoffsetpos[4] = H;
Yoffsetpos[4] = 0;
}

int ball()
{

  /* This loop finds each balls position according to the equations, 
   * then checks if their positions are where the renderer is currently at.
   * It goes in order of Ball 1, 2 ,3 ,4 ........ and so on for however many 
   * balls their are. */

int i = 0;

  for(i = 0; i < BallsInSystem; i++)
  {

    BallX[i] = AccelerationEquX(grav, time[i], vlctyX[i]);
    BallY[i] = VelocityY(vlctyY[i], time[i]);

    XChkBounds(i);
    YChkBounds(i);


      if((BallX[i] - Xoffsetpos[i]) == x && (BallY[i] + Yoffsetpos[i]) == y)
      {
        printf("\u2588");
      }
  }

}

void Renderer()
{


  for(x = 0; x > -H; x--){

    printf("\n");

    for(y = 0; y < W; y++){

      // Background
      printf(" ");

      // Foreground
      ball();

    }
  }

}

main()
{
  initialize();


while(1)
{
frames++;
  time[0] = time[0] + 0.05;
  time[1] = time[1] + 0.05;
  time[2] = time[2] + 0.05;
  time[3] = time[3] + 0.05;
  time[4] = time[4] + 0.05;

  usleep(20000);

  Renderer();
  printf("\e[3J\e[H\e[2J");
  printf("Ball X: %d Ball Y: %d", BallX[0], (BallY[0] + Yoffsetpos[0]));
  printf(" Time: %f", time[0]);
  printf(" Frames: %d", frames);
  printf(" Y velocty: %f", vlctyY[0]);
  printf(" Y offset: %d,", Yoffsetpos[0]);
}

  return 0;
}
